package Web;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

public class ThunderSearch
{
    int numDocs;
    Retriever articleRetriever;
    Retriever titleRetriever;

    public ThunderSearch(Retriever articleRetriever, Retriever titleRetriever, int numDocs) throws Exception
    {
        this.articleRetriever = articleRetriever;
        this.titleRetriever = titleRetriever;
        this.numDocs = numDocs;
    }
    
    /**
     * Converts a double to a string with two-digit precision.
     * @param d - the double to convert
     * @return the String representing the parameter
     */
    private String round2(double d)
    {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return twoDForm.format(d).toString();
    }

    public String getRankedResults(String query) throws Exception
    {
        double[] articleRanks = getRanks(query, articleRetriever);
        double[] titleRanks = getRanks(query, titleRetriever);
        Result[] results = new Result[numDocs];

        // generate a combined ranking
        for(int i = 0; i < numDocs; ++i)
        {
            results[i] = new Result();
            results[i].articleRank = articleRanks[i];
            results[i].titleRank = titleRanks[i];
            results[i].pageRank = Math.log10(StaticData.pageRanks[i]);
            results[i].combinedScore = results[i].articleRank + results[i].titleRank + results[i].pageRank;
            results[i].title = StaticData.titleArray[i];
        }

        HashSet<String> unwantedPrefixes = new HashSet<String>();
        unwantedPrefixes.add("Wikipedia:");
        unwantedPrefixes.add("Category:");
        unwantedPrefixes.add("Portal:");
        unwantedPrefixes.add("Template:");
        unwantedPrefixes.add("Book:");
        unwantedPrefixes.add("File:");
        unwantedPrefixes.add("Help:");
        unwantedPrefixes.add("MediaWiki:");

        System.out.println("[ThunderSearch]: sorting results");
        Arrays.sort(results, new Comparator<Result>() {
            @Override
            public int compare(Result r1, Result r2) {
                return (r1.combinedScore < r2.combinedScore) ? 1 : 0;
            }
        });

        System.out.println("[ThunderSearch]: trimming duplicate entries");
        
        HashSet<String> set = new HashSet<String>();
        ArrayList<Result> displayedResults = new ArrayList<Result>();
        for(int i = 0; i < results.length && displayedResults.size() < 20; ++i)
        {
            String title = results[i].title;
            int prefixIndex = title.indexOf(':');
            if(!unwantedPrefixes.contains(title.substring(0, prefixIndex + 1)))
            {
                if(!set.contains(getCleanWords1(title)) && !set.contains(getCleanWords2(title)))
                {
                    displayedResults.add(results[i]);
                    set.add(getCleanWords1(title));
                    set.add(getCleanWords2(title));
                }
            }
        }
        
        return generateHTML(displayedResults, query);
    }
    
    private String generateHTML(ArrayList<Result> displayedResults, String query)
    {
        // header
        String output = "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">";
        output += "<html><head></head><title>ThunderSearch</title>";
        output += "<center><h1>ThunderSearch</h1></center>";
        output += "<body><center><form method=get action=\"ThunderSearch\">";
        output += "<input type=text name=\"query\" size=80>";
        output += "<input type=submit value=Search></form></center></body></html>";

        // results table headers
        output += "<br/><p>Displaying results for the query \"" + query + "\"</p>";
        output += "<table border=\"0\" width=\"100%\"><tr>";
        output += "<th align=\"left\"><b>CombinedScore</b></th>";
        output += "<th align=\"left\"><b>ArticleRank</b></th>";
        output += "<th align=\"left\"><b>TitleRank</b></th>";
        output += "<th align=\"left\"><b>PageRank</b></th>";
        output += "<th align=\"left\"><b>ArticleTitle</b></th></tr>";
        
        // each result's row
        for(Result r: displayedResults)
        {
            output += "<tr>";
            output += "<td>" + round2(r.combinedScore) + "</td>";
            output += "<td>" + round2(r.articleRank) + "</td>";
            output += "<td>" + round2(r.titleRank) + "</td>";
            output += "<td>" + round2(r.pageRank) + "</td>";
            String linkTitle = r.title.replaceAll(" ", "_");
            output += "<td>" + "<a href=\"http://www.en.wikipedia.org/wiki/" + linkTitle + "\">" + r.title + "</a>" + "</td>";
            output += "</tr>";
        }
        
        output += "</table></body></html>";
        return output;
    }

    private static String[] getArrayOfWords(String pageText)
    {
        return pageText.toLowerCase().replaceAll("[[\\W]&&[\\S]]", " ").split("\\s");
    }

    private static String getCleanWords1(String pageText)
    {
        return pageText.toLowerCase().replaceAll("[[\\W]&&[\\S]]", "");
    }

    private static String getCleanWords2(String pageText)
    {
        return pageText.toLowerCase().replaceAll("[[\\W]&&[\\S]]", " ");
    }

    private double[] getRanks(String query, Retriever r) throws Exception
    {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        double[] result = new double[numDocs];
        String[] words = getArrayOfWords(query);
        for(int i = 0; i < words.length; ++i)
        {
            if(!map.containsKey(words[i]))
                map.put(words[i], 0);
            map.put(words[i], map.get(words[i]) + 1);
        }
        for(String word: map.keySet())
        {
            if(word.length() > 0)
                getRanksForWord(result, word, map.get(word), r);
        }
        return result;
    }

    private void getRanksForWord(double[] result, String word, int qtf, Retriever r) throws Exception
    {
        int[][] docs = r.retrieve(word);
        
        if(docs == null)
        {
            System.out.println("[ThunderSearch]: Word not found! (" + word + ")");
            System.exit(1);
        }

        System.out.println("[ThunderSearch]: Getting ranks for \"" + word + "\" in " + r);
        int df = docs.length;
        for(int i = 0; i < df; i++)
            getRanksForWordAndDoc(result, word, qtf, docs[i], df, r.getLengths(), r.getAvdl());
    }

    private void getRanksForWordAndDoc(double[] ranks, String word, int qtf, int[] doc, int df, int[] lengths, double av)
            throws Exception
    {
        int docID = doc[0];
        double tf = doc[1];
        double dl = lengths[docID];

        double k1 = 1.5;
        double b = .75;
        double k3 = 500;

        double TF = ((k1 + 1.0) * tf) / ((k1 * ((1.0 - b) + b * dl / av)) + tf);
        double IDF = (((double) numDocs) - df + 0.5) / (df + .05);
        double QTF = ((k3 + 1.0) * qtf) / (k3 + qtf);

        ranks[docID] += Math.log(IDF) * TF * QTF;
    }
}