package Web;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Retriever
{
    RandomAccessFile indices;
    int lengths[];
    double avdl = 0;
    String[] words;
    long[] offsets;
    int numDocs;
    String dir;

    public int[] getLengths()
    {
        return lengths;
    }

    public double getAvdl()
    {
        return avdl;
    }

    public Retriever(String dir, int numDocs) throws Exception
    {
        this.dir = dir;
        this.numDocs = numDocs;
        lengths = new int[numDocs];
        words = new String[numDocs];
        offsets = new long[numDocs];
        String index = dir + "/InvertedIndex.txt";
        String pointers = dir + "/Pointers.txt";
        String lengthsFile = dir + "/Lengths.txt";

        calculateLengths(lengthsFile);

        System.out.println("[Retriever]: Initializing inverted index in " + dir);
        indices = new RandomAccessFile(index, "r");

        /*
         * Here we load our pointer file into memory for quick access, it gets
         * loaded into words and offsets
         */
        BufferedReader br = new BufferedReader(new FileReader(pointers));
        int i = 0;
        while(br.ready())
        {
            String s = br.readLine();
            String[] line = s.split("\\s");
            words[i] = line[0];
            offsets[i] = Long.parseLong(line[1]);
            ++i;
        }
    }

    private void calculateLengths(String lengthsFile) throws Exception
    {
        BufferedReader br = new BufferedReader(new FileReader(lengthsFile));
        for(int i = 0; i < numDocs; i++)
        {
            lengths[i] = Integer.parseInt(br.readLine());
            avdl += lengths[i];
        }
        avdl /= numDocs;
    }

    /**
     * A helper function and debugging function that returns the index of the
     * (word, offset) pair that the querying word comes after
     * 
     * @param word
     * @return index
     */
    public int isAfter(String word)
    {
        for(int i = 0; i < words.length; i++)
        {
            if(word.compareTo(words[i]) < 0)
                return i - 1;
        }
        return words.length - 1;
    }

    /**
     * Loads the correct chunk of the index into memory, then uses String
     * operations to quickly (but linearly) look through the file to find the
     * part that has the word that we are looking for
     * 
     * @param word
     * @return
     * @throws IOException
     */
    public int[][] retrieve(String word) throws IOException
    {
        int i = isAfter(word);
        long startOffset = offsets[i];
        long toRead;
        if(i == offsets.length - 1)
            toRead = indices.length() - startOffset;
        else
            toRead = offsets[i + 1] - startOffset;
        indices.seek(startOffset);

        byte[] data = new byte[(int) (toRead)];

        indices.read(data);

        // make sure we can find the first word if it's the one we want
        String dataStr = new String(data);
        dataStr = "\r\n" + dataStr;

        int offset = dataStr.indexOf("\r\n" + word + "\r\n");
        if(offset == -1)
        {
            System.out.println(word + ": term not found.");
            return null;
        }
        else
        {
            offset += 2; // get to the actual word, not the "\r\n"
            int end = dataStr.indexOf("\r\n" + "\r\n", offset);
            String str = dataStr.substring(offset, end);
            return getData(str);
        }
    }

    /**
     * Takes the entry for a word in the inverted index and returns an int[][]
     * array of documents and their counts
     * 
     * @param str
     * @return
     */
    private int[][] getData(String str)
    {
        String[] lines = str.split("\r\n");
        int[][] ret = new int[lines.length - 2][2];
        for(int i = 0; i < lines.length; i++)
        {
            if(i > 1)
            {
                String[] parts = lines[i].split("\t");
                ret[i - 2][0] = Integer.parseInt(parts[0]);
                ret[i - 2][1] = Integer.parseInt(parts[1]);
            }
        }
        return ret;
    }

    public String toString()
    {
        return dir;
    }
}
