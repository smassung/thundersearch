package Web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Index extends HttpServlet
{
    private static final long serialVersionUID = 2L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter writer = response.getWriter();
        String output = "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML//EN\">";
        output += "<html><head></head><title>ThunderSearch</title>";
        output += "<center><h1>ThunderSearch</h1></center>";
        output += "<body><center><form method=get action=\"ThunderSearch\">";
        output += "<input type=text name=\"query\" size=80>";
        output += "<input type=submit value=Search></form></center></body></html>";
        writer.write(output);
    }
}