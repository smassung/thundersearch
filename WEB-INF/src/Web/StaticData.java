package Web;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class holds data that does not need to be computed at run time.
 * It should be called in the init() function so it can be loaded only once.
 * @author sean
 *
 */
public class StaticData
{
    static double[] pageRanks;
    static String[] titleArray;
    
    public static void load(String titles, String ranks, int numDocs) throws IOException
    {
        titleArray = new String[numDocs];
        pageRanks = new double[numDocs];
    
        BufferedReader titleReader = new BufferedReader(new FileReader(titles));
        BufferedReader rankReader = new BufferedReader(new FileReader(ranks));
        System.out.println("[StaticData]: reading PageRank data and ArticleTitle data");
        for(int i = 0; i < numDocs; i++)
        {
            titleArray[i] = titleReader.readLine();
            pageRanks[i] = Double.parseDouble(rankReader.readLine());
        }
    }
}
