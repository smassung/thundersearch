package Web;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class Search extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    ThunderSearch search;

    /**
     * Initialize the indexes, and get ready for queries.
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        int numDocs = 11930681;
        String pathPrefix = "/home/sean/projects/cs410-final-data/";

        // create index retrievers
        Retriever r1 = null;
        Retriever r2 = null;
        try
        {
            r1 = new Retriever(pathPrefix + "ArticleIndex", numDocs);
            r2 = new Retriever(pathPrefix + "TitleIndex", numDocs);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        // create a ThunderSearch instance
        search = null;
        try
        {
            StaticData.load(pathPrefix + "ListOfTitles.txt", pathPrefix + "PageRanks.txt", numDocs);
            search = new ThunderSearch(r1, r2, numDocs);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Receive a query and return a ranked list of results.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String[] queryTerms = request.getParameterValues("query");
        String query = "";
        for(String s : queryTerms)
            query += s + " ";
        query = query.substring(0, query.length() - 1);
        String output = "<p>Failed to find search results!</p>";

        try
        {
            output = search.getRankedResults(query);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        PrintWriter out = response.getWriter();
        out.write(output);
        out.close();
    }
}