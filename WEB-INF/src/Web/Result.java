package Web;

/**
 * Represents a search result so it can be passed to different parts of ThunderSearch.
 * @author sean
 *
 */
public class Result
{
    public double combinedScore;
    public double articleRank;
    public double titleRank;
    public double pageRank;
    public String title;
    
    public String toString()
    {
        return "Score: " + combinedScore + "; AR: " + articleRank
                + " TR: " + titleRank + " PR: " + pageRank + " " + title;
    }
}
